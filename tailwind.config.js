const plugin = require('tailwindcss/plugin')
const {colors} = require('tailwindcss/defaultTheme')
const {fontSize} = require('tailwindcss/defaultTheme')


module.exports = {
    theme: {
        minWidth: {
            '0': '0',
            '1/4': '25%',
            '1/2': '50%',
            '3/4': '75%',
            'full': '100%',
        },
        fontFamily: {
            'arial': ['Arial']
        },
        fontSize: {
            ...fontSize,
            'banner': '2.5rem',
        },
        extend: {
            colors: {
                blue: {
                    ...colors.blue,
                    '500': '#005EB8',
                    '600': '#164C89',
                },
                red: {
                    ...colors.red,
                    '600': '#A30303',
                },
                gray: {
                    ...colors.gray,
                    '200': '#F0F4F5',
                }
            }
        }
    },
    variants: {
        textColor: ['responsive', 'hover', 'focus', 'visited', 'disabled', 'invalid'],
    },
    plugins: [
        plugin(function ({addVariant, e}) {
            addVariant('invalid', ({modifySelectors, separator}) => {
                modifySelectors(({className}) => {
                    return `.${e(`invalid${separator}${className}`)}:invalid`
                })
            })
        })
    ],
}

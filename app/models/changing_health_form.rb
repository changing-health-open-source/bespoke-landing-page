class ChangingHealthForm
  include ActiveModel::Validations

  attr_accessor :first_name, :last_name, :email, :day, :month, :year, :gender, :postcode, :best_type, :how, :policy, :request_failure, :date_of_birth, :email_exists

  validates_presence_of :first_name
  validate :special_characters_first_name
  validates_length_of :first_name, minimum: 1, maximum: 50
  #validates_format_of :first_name, with: /\A[a-zA-Z]/, message: 'needs to start with a letter', :allow_blank => true
  #validates_format_of :first_name, with: /[a-zA-Z]\z/, message: 'needs to end with a letter', :allow_blank => true

  validates_presence_of :last_name
  validate :special_characters_last_name
  validates_length_of :last_name, minimum: 1, maximum: 50
  #validates_format_of :last_name, with: /\A[a-zA-Z]/, message: 'needs to start with a letter', :allow_blank => true
  #validates_format_of :last_name, with: /[a-zA-Z]\z/, message: 'needs to end with a letter', :allow_blank => true

  validates_format_of :email,
  :with => RFC822::EMAIL,
  :message => 'Enter an email address in the correct format, like name@example.com'

  validates_presence_of :day
  validates_numericality_of :day, greater_than_or_equal_to: 1, less_than_or_equal_to: 31, allow_blank: true

  validates_presence_of :month
  validates_numericality_of :month, greater_than_or_equal_to: 1, less_than_or_equal_to: 12, allow_blank: true

  validates_presence_of :year
  validates_numericality_of :year, greater_than_or_equal_to: 1900, less_than_or_equal_to: Time.now.year, allow_blank: true

  validate :valid_date

  validates_presence_of :gender, message: 'You must select an answer.'


  validates_presence_of :postcode
  #validates_length_of :postcode, minimum: 3, maximum: 8, allow_blank: true
  validates_format_of :postcode,
  :with =>  /^([A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW])\s?[0-9][ABD-HJLNP-UW-Z]{2}|(GIR\ 0AA)|(SAN\ TA1)|(BFPO\ (C\/O\ )?[0-9]{1,4})|((ASCN|BBND|[BFS]IQQ|PCRN|STHL|TDCU|TKCA)\ 1ZZ))$$/i,
  :message => "Enter a real postcodes",
  :multiline => true

  validates_presence_of :best_type, message: 'You must select an answer.'


  validates_presence_of :how, message: 'You must select an answer.'

  validates :policy, inclusion: { in: ["1"], message: 'You will need to agree to the privacy policy to continue with registration.' }

  def populate(data)
    data.each do |key, value|
      self.instance_variable_set("@#{key.to_sym}", value.strip)
    end
  end

  def changing_health_request
    ChangingHealthRequest.new self.instance_values
  end

  def special_characters_first_name
    two_special_characters(string: first_name, field: :first_name)
  end

  def special_characters_last_name
    two_special_characters(string: last_name, field: :last_name)
  end

  def valid_date
    return true if day.empty? && month.empty? && year.empty?
    begin
    Date.civil(self.year.to_i, self.month.to_i, self.day.to_i)
    rescue ArgumentError
      errors.add(:date_of_birth, "Date of birth must be a real date.")
      false
    end
  end

  def not_old_enough?
    return false if day.nil? && month.nil? && year.nil?
    begin
    date = Date.civil(self.year.to_i, self.month.to_i, self.day.to_i)
    rescue ArgumentError
      return false
    end
    date > 18.years.ago.to_date
  end

  private
  def only_one_special_character(string:, field:)
    return true if string.empty?
    valid = true
    ". -'".split('').each do |character|
      if string.to_s.count(character) > 1
        errors.add(field, "only one #{character == ' ' ? 'space':character} allowed")
        valid = false
      end
    end
    valid
  end

  def two_special_characters(string:, field:)
    return true if string.empty?
    valid = true
    ". -``'".split('').each do |character|
      if string.to_s.count(character) > 2
        #errors.add(field, "only one #{character == ' ' ? 'space':character} allowed")
        errors.add(field, "Last name should include no more than 2 special characters. Special characters include space, hyphen, dot, single quote, and apostrophe.")
        valid = false
      end
    end
    valid
  end

end

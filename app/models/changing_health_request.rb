class ChangingHealthRequest
  include ActiveModel::Validations

  attr_accessor :sys_first_name, :sys_last_name, :email, :unit_id, :package_id, :enabled, :welcome_message, :birthday, :pops

  def initialize(data)
    @sys_first_name = data['first_name']
    @sys_last_name = data['last_name']

    @email = data['email'].downcase
    #Rails.logger.info "Email sent to API: #{@email}"

    @unit_id = Rails.application.config.unit_id
    @package_id = Rails.application.config.package_id

    @enabled = true
    @welcome_message = true
    @birthdate = "#{data['year'].to_i}-#{"%02d" % data['month'].to_i}-#{"%02d" % data['day'].to_i}"
    @pops = []
    pops << add_pop(category: "gender", key: "gender-m-f-i-p", value: data['gender'])
    pops << add_pop(category: "address", key: "postcode", value: data['postcode'])
    pops << add_pop(category: "diabetes-status", key: "diabetes-status-healthy-living-update", value: data['best_type'])
    pops << add_pop(category: "other-details", key: "find-out-about-healthy-living", value: data['how'])
    pops << add_pop(category: "consent", key: "i-agree", value: data['policy']=='1' ? 'True':'False')
  end

  def add_pop(category:, key:, value:)
    {
        "category" => category,
        "key" => key,
        "value" => value
    }
  end
end

class KnowdiabetesController < ApplicationController
  include ChangingHealthHelper
  layout 'knowdiabetes'

  before_action :load_classes

  # TODO: Remove after testing is ok
  # Also remove key from links!
  def check_if_debug
    unless params[:key] == "sKJMjt4mfLYqWVbJ"
      redirect_to "/"
      true
    else
      false
    end

  end

  def index
    # Remove check_if_debug after it is tested
    return if check_if_debug
    redirect_to block_path unless geo_pass?

    @know_diabetes_form = KnowDiabetesForm.new
  end

  def create
    redirect_to block_path unless geo_pass?

    # render text: "Thanks for sending a POST request with cURL! Payload: #{request.body.read}
    @know_diabetes_form = KnowDiabetesForm.new
    @know_diabetes_form.populate params[:knowdiabetesform]

    unless @know_diabetes_form.valid? && !@know_diabetes_form.not_old_enough?
      respond_to do |format|
        format.json { render json: @know_diabetes_form.errors }
        format.html { render "index" }
      end
      return
    end
    api_response = send_request @know_diabetes_form.know_diabetes_request
    if api_response == SUCCESSFUL
      redirect_to '/knowdiabetes/registered'
    elsif api_response == EMAIL_EXISTS
      @know_diabetes_form.email_exists = true
      render "index"
    else
      @know_diabetes_form.request_failure = true
      render "index"
    end
  end

end
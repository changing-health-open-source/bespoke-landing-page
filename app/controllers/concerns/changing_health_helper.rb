module ChangingHealthHelper
  GENERAL_ERROR = -1
  SUCCESSFUL = 0
  EMAIL_EXISTS = 1
  NOT_OLD_ENOUGH = 2

  def geo_pass?
    # Uncomment next line to disable geo-location block
    # return true
    # whitelist
    whitelist = ['31.1.163.3', '8.40.107.68']
    return true if whitelist.include? request.remote_ip

    country = ip_country(request.remote_ip)
    country == 'United Kingdom of Great Britain and Northern Ireland' || country == 'United Kingdom'
  end

  def ip_country(request_ip)
    begin
      uri = URI("https://pro.ip-api.com/json/#{request_ip}?key=#{Rails.application.credentials.ip_api_key}")
      ip_request = Net::HTTP::Get.new(uri)

      ip_response = Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https', :open_timeout => 2, :read_timeout => 2) { |https|
        https.request(ip_request)
      }
    rescue Net::ReadTimeout, Net::OpenTimeout => exception
      STDERR.puts "pro.ip-api.com is NOT reachable. Using backup service"
      return backup_ip_country(request_ip)
    end

    if ip_response.kind_of? Net::HTTPSuccess
      ip_json = JSON.parse(ip_response.body)

      return ip_json['country']
    else
      STDERR.puts "ip-api returned error. Using backup service"
      return backup_ip_country(request_ip)
    end
  end

  def backup_ip_country(request_ip)
    require 'ip2location_ruby'

    i2l = Ip2location.new.open('../db/IP2LOCATION-LITE-DB3.IPV6.BIN')
    record = i2l.get_all(request_ip)

    return record && record['country_long'] != '-' && record['country_long'] || 'unknown location'
  end

  def registered
    redirect_to block_path unless geo_pass?
  end

  def block
    @country = ip_country(request.remote_ip)
  end

  def privacy
    redirect_to block_path unless geo_pass?
  end

  def load_classes
    @h1_classes = 'mt-4 mb-4 font-bold text-5xl lg:text-xl lg:text-base'
    @h1_classes_blue = @h1_classes + ' text-blue-600'
    @h1_classes_light_blue = @h1_classes + ' text-blue-500'
    @h1_classes_red = @h1_classes + ' text-red-600'

    @h2_classes = 'mt-4 mb-4 font-medium text-5xl lg:text-base'
    @h2_classes_blue = @h2_classes + ' text-blue-500'

    @h3_classes = 'mt-4 mb-4 font-medium text-4xl lg:text-base'
    @h3_classes_blue = @h3_classes + ' text-blue-600'
    @h3_classes_red = @h3_classes + ' text-red-700'

    @normal_text = 'font-normal text-4xl lg:text-sm mx-2'
    @normal_text_gray = @normal_text + ' text-gray-800'
    @normal_text_gray_with_margin = @normal_text_gray + ' mb-4'

    @small_text = 'font-normal text-2xl lg:text-xs'
    @small_text_gray = @small_text + ' text-gray-800'

    @table = 'table-auto border-collapse border-2 border-gray-500 mb-4 text-center'
    @table_header = 'border border-gray-400 px-4 py-2 text-2xl lg:text-xs'
    @table_cell = 'border border-gray-400 px-4 py-2 text-2xl lg:text-xs font-thin'

    @container = 'bg-white w-full lg:w-9/12 mx-auto p-4 lg:p-8 rounded shadow-xl lg:max-w-5xl'

    @button = 'bg-blue-700 hover:bg-blue-800 text-white px-5 lg:px-8 py-4 lg:py-2 border rounded-full uppercase font-bold text-3xl lg:text-sm'
    @label = 'block text-gray-700 text-3xl lg:text-xs font-bold'
    @sub_label = 'block text-gray-700 text-2xl lg:text-xs font-normal'
    @checkbox_label = 'select-none text-3xl lg:text-xs'
    @text_field = 'shadow appearance-none border border-blue-500 rounded w-full pt-4 lg:pt-2 pb-2 lg:pb-1 text-3xl lg:text-sm px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
    @date_field = 'shadow border border-blue-500 rounded pt-2 lg:pt-1 pb-2 lg:pb-1 text-3xl lg:text-sm px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline flex-shrink'

    @text_light = 'text-3xl lg:text-xs font-light leading-tight'

    @select = 'block appearance-none w-full border border-blue-500 pt-4 lg:pt-2 pb-2 lg:pb-1 px-4 pr-8 text-3xl lg:text-sm leading-tight focus:outline-none focus:shadow-outline text-gray-700 invalid:text-gray-500'
    @date = 'shadow appearance-none border border-blue-500 rounded px-1 lg:px-2 pt-4 lg:pt-1 pb-2 lg:pb-1 text-3xl lg:text-sm text-gray-700 leading-tight focus:outline-none focus:shadow-outline'
  end

  private

  def send_request(unified_request)
    uri = URI(Rails.application.config.api_endpoint)

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true

    headers = {
        'Content-Type' => 'application/json',
        'Accept' => '/application/x.changinghealth.v1+json',
        'Authorization' => 'Bearer ' + Rails.application.credentials.changing_health_token
    }

    request = Net::HTTP::Post.new(uri.path, headers)

    unified_request = unified_request
    request.body = unified_request.to_json

    # request
    response = http.request(request)

    if response.kind_of? Net::HTTPSuccess
      SUCCESSFUL
    else

      Rails.logger.info "Error API Request received"
      body = JSON.parse (response.body)
      if body['errors']['email']&.first == "Email hash exists"
        return EMAIL_EXISTS
      end
      errors = body['errors'].to_s
      PageMailer.with(response: response.message, errors: errors).api_error.deliver_later
      GENERAL_ERROR
    end
  end
end
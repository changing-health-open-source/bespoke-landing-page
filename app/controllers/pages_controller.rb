class PagesController < ApplicationController
  include ChangingHealthHelper

  before_action :load_classes

  def index
    redirect_to block_path unless geo_pass?

    @changing_health_form = ChangingHealthForm.new
  end

  def create
    redirect_to block_path unless geo_pass?

    # render text: "Thanks for sending a POST request with cURL! Payload: #{request.body.read}
    @changing_health_form = ChangingHealthForm.new
    @changing_health_form.populate params[:changinghealthform]

    unless @changing_health_form.valid? && !@changing_health_form.not_old_enough?
      respond_to do |format|
        format.json {render json: @changing_health_form.errors}
        format.html {render "index"}
      end
      return
    end
    api_response = send_request @changing_health_form.changing_health_request
    if api_response == SUCCESSFUL
      redirect_to '/registered'
    elsif api_response == EMAIL_EXISTS
      @changing_health_form.email_exists = true
      render "index"
    else
      @changing_health_form.request_failure = true
      render "index"
    end
  end

end

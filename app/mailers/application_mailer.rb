class ApplicationMailer < ActionMailer::Base
  default from: 'admin@healthyliving.nhs.uk'
  layout 'mailer'
end

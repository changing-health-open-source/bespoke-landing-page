class PageMailer < ApplicationMailer
  def api_error
    Rails.logger.info "Error API Request notification - started"
    @response = params[:response]
    @errors = params[:errors]
    # @request = params[:request]
    #TODO: Make better email template
    mail(to: 'kw@quantavi.com,tim@changinghealth.com', subject: "HealthyLiving: Request received error response")
  end
end

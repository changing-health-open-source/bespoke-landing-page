module HomeHelper
  def alphanumeric
    "return (event.charCode > 64 && event.charCode < 91) ||
    (event.charCode > 96 && event.charCode < 123) || (event.charCode === 45) ||
   (event.charCode === 39) || (event.charCode === 32) || (event.charCode === 46)"
  end

  def numbers
    "return (event.charCode > 47 && event.charCode < 58)"
  end

  def max_length(length)
    "if (this.value.length > #{length}) {
          this.value = this.value.slice(0,#{length});
      };
    "
  end

  def max_value(value)
    "if (this.value > #{value}) {
      this.value = #{value}
     };
     "
  end

  def min_value(value)
    "if (this.value < #{value}) {
      this.value = #{value}
     };
     "
  end
end

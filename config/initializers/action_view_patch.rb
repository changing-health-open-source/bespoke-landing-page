module ActionView
  module Helpers
    module Tags # :nodoc:
      class Base # :nodoc:
        def select_content_tag(option_tags, options, html_options)
          html_options = html_options.stringify_keys
          add_default_name_and_id(html_options)

          value = options.fetch(:selected) { value() }
          select = content_tag("select", add_options(option_tags, options, value), html_options)

          if html_options["multiple"] && options.fetch(:include_hidden, true)
            tag("input", disabled: html_options["disabled"], name: html_options["name"], type: "hidden", value: "") + select
          else
            select
          end
        end
      end
    end
  end
end
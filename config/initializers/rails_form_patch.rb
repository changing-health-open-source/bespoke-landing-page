class ActionView::Helpers::FormBuilder
  def error_message_for(field_name)
    if self.object.errors[field_name].present?
      model_name              = self.object.class.name.downcase
      id_of_element           = "error_#{model_name}_#{field_name}"
      target_elem_id          = "#{model_name}_#{field_name}"
      class_name              = 'text-3xl lg:text-xs font-light leading-tight text-red-700 mt-1'
      error_declaration_class = 'border-red-700'
      error_text_class = 'text-red-700'
      noerror_class = 'border-blue-500'

      "<div id=\"#{id_of_element}\" for=\"#{target_elem_id}\" class=\"#{class_name}\">"\
      "#{self.object.errors[field_name].join(', ')}"\
      "</div>"\
      "<!-- Later JavaScript to add class to the parent element -->"\
      "<script>"\
          "(function(){"\
          "document.getElementById('#{target_elem_id}').classList.remove('#{noerror_class}');"\
          "document.getElementById('#{target_elem_id}').classList.add('#{error_declaration_class}');"\
          "document.getElementById('#{target_elem_id}').classList.add('#{error_text_class}');"\
          "document.getElementById('#{target_elem_id}').labels[0].classList.add('#{error_text_class}');"\
          "})();"\
      "</script>".html_safe
    end
  rescue
    nil
  end
end
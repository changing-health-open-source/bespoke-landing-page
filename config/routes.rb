Rails.application.routes.draw do
  get 'block', to: 'pages#block'
  get 'registered', to: 'pages#registered'
  get 'privacy', to: 'pages#privacy'
  resource :pages, only: [:index, :create]

  get 'pages' => 'pages#index'
  root 'pages#index'
  # NWL subpage
  resource :knowdiabetes, only: [:index, :create]
  get 'knowdiabetes' => 'knowdiabetes#index'
  scope '/knowdiabetes' do
    get 'block', to: 'knowdiabetes#block'
    get 'registered', to: 'knowdiabetes#registered'
    get 'privacy', to: 'knowdiabetes#privacy'
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end

#Important:
Don't push changes from server!

#Tips
* When deploying : 
    * before restart do _bundle install_
    
#Connecting:
ssh -i HealthyLiving.pem ubuntu@healthyliving.nhs.uk

#Info:
* dir: /var/app/landingpage/
* After changes to js run:
    * _RAILS_ENV=production rails assets:precompile_
    
#Run:
bin/webpack-dev-server 

#Restart server:
sudo service apache2 restart

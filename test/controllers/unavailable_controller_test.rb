require 'test_helper'

class UnavailableControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get unavailable_index_url
    assert_response :success
  end

end

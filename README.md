# README

This is a source code of a Landing Page that allows users to send request to API that handles registration to Changing Health Application.
* License - MIT - please check LICENSE file

* Ruby version
    
    2.6.5
* System dependencies

    Linux system with Apache and Passenger Module
* Configuration

* Database creation
    
    No database at this moment. Requests are handled ad-hoc

* Running it

    * Install ruby 2.6.5
    * Install Rails 6.0.2
    * Clone the repository
    * Setup Apache site, use standard configuration, remembering to use PassengerMod
    * The page should be on-line
    
* Development
    * remember to disable GeoLocation service in pages_controller.rb by uncommenting following line:
      
      record = i2l.get_all('84.17.51.144') # England
